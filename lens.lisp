(defpackage :fresnel/lens
  (:documentation
   "Lenses for bidirectional transformations.

As defined by Foster et al., 2007; Foster et al., 2008; Bohannon et
al., 2008; and Foster et al., 2009.

It is not recommended to :USE this package; use a package-local
nickname:

    (:local-nicknames (:l :fresnel/lens))

The convention is for lens constructors to be functions, even if they
take no arguments (such as IDENTITY). This is to be consistent with
other, parametric lens constructors. For example, CAR can be called
with either zero or one argument rather than having a function that
takes one argument and a non-function for the zero-argument case.

The DISCERN and MATCH macros, and the CHECK, NILABLE, PATTERN, and OR
functions, deal with discerning lenses. A discerning lens is a quotient
lens that partitions its set of concrete structures (and similarly,
its set of abstract structures) into two subsets: a set of
\"well-formed\" values and a set of \"ill-formed\" values. While the
set of well-formed values can be equipped with any equivalence
relation as usual, all ill-formed values are considered equivalent to
each other.

For each of the three functions of a discerning lens, the following
must be true iff the (first) argument to the function was ill-formed:

- The first of the VALUES returned by the function is ill-formed.

- The last of the VALUES returned by the function is NIL.

By these conventions, for instance, the IDENTITY lens is discerning
with NIL as its only ill-formed value, but the CAR lens is not
discerning. The DISCERN macro can be wrapped around a usage of a
discerning lens in order to get both the primary return value and the
well-formedness together, as a single value (a cons).")
  (:use)
  (:import-from :trivial-package-local-nicknames)
  (:export
    #:acond
    #:after
    #:alist-mapping
    #:apply
    #:backward
    #:match-constructors
    #:before
    #:bij
    #:canonize
    #:canonizer
    #:canonize-before
    #:canonize-after
    #:car
    #:cdr
    #:check
    #:check
    #:check-laws
    #:check-subtype
    #:check-type
    #:choose
    #:combine
    #:compose
    #:compose*
    #:cons
    #:constantly
    #:create
    #:default
    #:discern
    #:discern-values
    #:forward
    #:fset-mapping
    #:get
    #:guard
    #:rguard
    #:hash-table-alist
    #:hash-table-mapping
    #:identity
    #:identity!
    #:lazy
    #:lazy-list
    #:lazy-list*
    #:lens
    #:lens-error
    #:list
    #:list*
    #:lquot
    #:lquot*
    #:make-lens
    #:map
    #:mapcar
    #:mapping
    #:match
    #:match-list
    #:match-list*
    #:nilable
    #:of-type
    #:opp
    #:optional
    #:or
    #:or!
    #:path
    #:pattern
    #:put
    #:qlens
    #:rquot
    #:rquot*
    #:satisfies
    #:seq
    #:some
    #:trace
    #:untrace
    #:with-component-lenses
    #:with-create-lenses
    #:with-defaults
    #:with-get-lenses
    #:with-put-lenses))

(defpackage :fresnel-user
  (:use :gt/full)
  (:local-nicknames (:l :fresnel/lens)))

;;; Package-inferred systems don't allow :lock.
#+sbcl
(cl:eval-when (:compile-toplevel :load-toplevel :execute)
  (sb-ext:lock-package :fresnel/lens))

(defpackage :fresnel/lens.impl
  (:use #:gt/full #:trivial-package-local-nicknames)
  #+sbcl (:implement :fresnel/lens)
  ;; (:local-nicknames (:l :fresnel/lens))
  (:import-from :fresnel/lens)
  (:documentation "Internal package where lenses are actually
  implemented. (Working in the lens package is inconvenient as it
  shadows many common functions)."))

(in-package :fresnel/lens.impl)

(eval-always
  (add-package-local-nickname :l :fresnel/lens))

(in-readtable :curry-compose-reader-macros)

(define-condition lens-error (simple-error)
  ((lens :initarg :lens :reader :lens-error-lens)))

(defmacro l:discern-values (form)
  "Return two values: the primary value of FORM, and its well-formedness.

FORM must evaluate to the result of calling one of the functions of a
discerning lens. The first value returned by this macro is the primary
value returned by FORM, and the second is a boolean indicating whether
that value is well-formed."
  (with-unique-names (fn)
    `(flet ((,fn () ,form))
       (declare (dynamic-extent #',fn))
       (discern-values* #',fn))))


(-> discern-values (function) (values t boolean &optional))
(defun discern-values* (fn)
  "Helper function for `discern-values'."
  (multiple-value-call
      (lambda (&rest vals)
        (declare (dynamic-extent vals))
        (values (first vals) (and (lastcar vals) t)))
    (funcall fn)))

(defmacro l:discern (form)
  "Return a cons cell of the primary value and well-formedness from FORM.

The FORM must evaluate to the result of calling one of the functions
of a discerning lens. The car of the cons cell returned by this macro
is the primary value returned by FORM, and the cdr is a boolean
indicating whether that value is well-formed."
  `(multiple-value-call #'cons (l:discern-values ,form)))

;;; NB The class hierarchy here is not necessary in terms of
;;; functionality, but having different classes for different purposes
;;; makes debugging easier and gives us a little type-safety.

(defclass l:qlens ()
  ((l:get
    :type (function (t) t)
    :reader get-fn
    :documentation
    "A function from C to A.")
   (l:put
    :type (function (t t) t)
    :reader put-fn
    :documentation
    "A function from A × C to C.")
   (l:create
    :type (function (t) t)
    :reader create-fn
    :documentation
    "A function from A to C.

Need not be defined on every lens; if it is unbound, then the CREATE function
simply throws an error."))
  (:documentation
   "Quotient lenses generalize lenses so that the lens laws are
   respected up to some equivalence relation.

Lenses may be \"quotiented\" arbitrarily with canonizers (a kind of
bijective lens) on the left (concrete) or right (abstract) side. This
allows a style where canonicalization is pervasive and interleaved
rather than being done in one step at the edges.

Practically they treat certain data as ignorable."))

(defmethod initialize-instance :after ((l l:qlens) &key get put create)
  ;; Creating bijective lenses is a common use case so it's convenient
  ;; to be able to just define GET and CREATE and let PUT have the
  ;; sensible default behavior of ignoring its concrete argument and
  ;; just using the result of CREATE.
  (when get
    (setf (slot-value l 'l:get) get))
  (when put
    (setf (slot-value l 'l:put) put)
    (unless create
      (setf (slot-value l 'l:create)
            (lambda (a)
              (l:put l a nil)))))
  (when create
    (setf (slot-value l 'l:create) create)
    (unless put
      (setf (slot-value l 'l:put)
            (lambda (a c)
              (declare (ignore c))
              (l:create l a))))))

(defclass l:lens (l:qlens)
  ()
  (:documentation
   "A basic lens.

Lenses are a kind of bidirectional transformation.

Lenses generalize bijections by allowing the transformation in the
forward direction (get) to be non-injective. (Put is still injective,
however.)

Lenses must satisfy the following laws:

- GetPut: (put l (get l c) c) = c for all c from C
- PutGet: (get l (put l a c)) = a for all (a, c) from A × C
- CreateGet: (get l (create l a)) = a for all a from A

Of course, the CreateGet law need not hold for a lens that does not have a
CREATE function."))

(defun l:check-laws (lens c a &key (test #'equal?))
  "Check that LENS obeys the lens laws for C, a concrete value, and A,
an abstract value, using TEST as the equivalence relation.

Returns four boolean values:
1. Whether all the laws were respected.
2. Whether GetPut was respected.
3. Whether PutGet was respected.
4. Whether CreateGet was respected."
  (fbind (test)
    ;; Run TEST with both argument orders.
    (let ((getput
            (and (test c (l:put lens (l:get lens c) c))
                 (test (l:put lens (l:get lens c) c) c)))
          (putget
            (and (test a (l:get lens (l:put lens a c)))
                 (test (l:get lens (l:put lens a c)) a)))
          (createget
            (and (test a (l:get lens (l:create lens a)))
                 (test (l:get lens (l:create lens a)) a))))
      (values (and getput putget createget)
              getput
              putget
              createget))))

(defmethod print-object ((self l:lens) stream)
  ;; Printing the functions is useful for debugging, as we may be able
  ;; to see where they were defined.
  (print-unreadable-object (self stream :type t)
    (with-slots (l:get l:put l:create) self
      (format stream "~a" (list l:get l:put l:create)))))

(defmethod convert ((to-type (eql 'l:qlens))
                    (l l:lens)
                    &key)
  (make 'l:qlens
        :get (get-fn l)
        :put (put-fn l)
        :create (create-fn l)))

(defclass l:bij (l:lens)
  ()
  (:documentation "Every bijection is also a lens, using a put
  function that ignores its second arg."))

(defmethod print-object ((self l:bij) stream)
  ;; Printing the functions is useful for debugging.
  (print-unreadable-object (self stream :type t)
    (with-slots (l:get l:create) self
      (format stream "~a" (list l:get l:create)))))

(defclass l:canonizer (l:bij)
  ((l:get
    :initarg :canonize :reader canonize-fn
    :documentation
    "Function that maps an element to its canonical representative.")
   (l:create
    :initarg :choose :reader choose-fn
    :documentation
    "Function that maps a canonical representive to some element."))
  (:documentation "A canonizer is a kind of bijection.
A lens is \"quotiented\" with a canonizer, resulting in a quotient lens.

The canonize function of the canonizer is called on arguments to the
lens; the choose function of the canonizer is called on the values the
lens returns."))

(-> l:canonizer ((-> (t) t)
                 &optional (-> (t) t))
  (values l:canonizer &optional))
(defun l:canonizer (canonize &optional (choose #'identity))
  "Build a canonizer from two functions, CANONIZE and CHOOSE.
CANONIZE and CHOOSE are treated as discerning, with special behavior:
if they fail then the original values are passed through unchanged.
This makes writing canonizers easier, as you don't have to remember to
explicitly pass through irrelevant values."
  (fbind (canonize choose)
    (make 'l:canonizer
          :canonize (lambda (c)
                      (let ((a (l:discern (canonize c))))
                        (if (cdr a)
                            (values (car a) (cdr a))
                            c)))
          :choose (lambda (a)
                    (let ((c (l:discern (choose a))))
                      (if (cdr c)
                          (values (car c) (cdr c))
                          a))))))

(defun l:canonize-before (fn)
  (l:canonizer fn #'identity))

(defun l:canonize-after (fn)
  (l:canonizer #'identity fn))

(defun l:canonize (canonizer value)
  "Call the canonize function from CANONIZER on VALUE."
  (funcall (get-fn canonizer) value))

(defun l:choose (canonizer value)
  "Call the choose function from CANONIZER on VALUE."
  (funcall (create-fn canonizer) value))

(defmethod convert ((to-type (eql 'l:canonizer)) (l l:canonizer) &key)
  l)

(defmethod convert ((to-type (eql 'l:canonizer)) (l l:qlens) &key)
  (l:canonizer {l:get l} {l:create l}))

(-> l:lquot (l:canonizer l:qlens) (values l:qlens &optional))
(defun l:lquot (canonizer lens)
  "Coarsen the (concrete) domain of QLENS using CANONIZER.
Return a new q-lens.

Composes the get function of QLENS with the canonizer of Q and
composes the chooser of Q with the put/create function of QLENS.

When using `lquot', only concrete values are changed. The canonizer is
called directly on the concrete arguments to GET and PUT, while the
chooser is called on the concrete result of CREATE and PUT (if they
succeed)."
  (l:qlens (lambda (c)
             (multiple-value-bind (c c?)
                 (l:discern-values (l:canonize canonizer c))
               (and c? (l:get lens c))))
           (lambda (a c)
             (multiple-value-bind (c c?)
                 (l:discern-values (l:put lens a (l:canonize canonizer c)))
               (and c? (l:choose canonizer c))))
           (lambda (a)
             (multiple-value-bind (c c?)
                 (l:discern-values (l:create lens a))
               (and c? (l:choose canonizer c))))))

(-> l:rquot (l:qlens l:qlens) (values l:qlens &optional))
(defun l:rquot (canonizer lens)
  "Coarsen the (abstract) codomain of QLENS using CANONIZER.
Return a new q-lens.

Composes the choose function of CANONIZER with the get function of
QLENS, and composes the put/create function of QLENS with the
canonizer function of CANONIZER.

When using `rquot', only abstract values are changed. The canonizer is
called directly on the abstract arguments to PUT and CREATE, while the
chooser is called on the abstract result of GET (if it succeeds). "
  (l:qlens (lambda (c)
             (multiple-value-bind (a a?)
                 (l:discern-values (l:get lens c))
               (when a?
                 (l:choose canonizer a))))
           (lambda (a c)
             (multiple-value-bind (a a?)
                 (l:discern-values (l:canonize canonizer a))
               (when a?
                 (l:put lens a c))))
           (lambda (a)
             (multiple-value-bind (a a?)
                 (l:discern-values (l:canonize canonizer a))
               (when a?
                 (l:create lens a))))))

(-> l:rquot* (&rest l:qlens) (values l:qlens &optional))
(defun l:rquot* (&rest lenses)
  "Reduce lenses with `l:rquot', from the end."
  (reduce #'l:rquot
          lenses
          :from-end t))

(-> l:lquot* (&rest l:qlens) (values l:qlens &optional))
(defun l:lquot* (&rest lenses)
  "Reduce lenses with `l:lquot', from the end."
  (reduce #'l:lquot
          lenses
          :from-end t))

(defun l:after (&rest lenses)
  "Right quotient each lens in LENSES with the next lens.
That is, all but the first lens should be canonizers."
  (reduce (flip #'l:rquot) lenses))

(defun l:before (&rest lenses)
  "Left quotient each lens in LENSES with the previous lens.
That is, all but the last lens should be canonizers."
  (apply #'l:lquot* lenses))

(-> l:get (l:qlens t) t)
(defun l:get (l c)
  "Return an abstract view of C through the lens L.

Not analogous to CL:GET."
  (funcall (slot-value l 'l:get) c))

(-> l:put (l:qlens t t) t)
(defun l:put (l a c)
  "Return an updated version of C using the abstract view A through the lens L."
  (funcall (slot-value l 'l:put) a c))

(-> l:create (l:qlens t) t)
(defun l:create (l a)
  "Return a concrete value from the abstract view A through the lens L."
  (funcall (slot-value l 'l:create) a))

(defun l:forward (l c &key fail-on-error)
  (handler-bind ((error (lambda (e)
                          (declare (ignore e))
                          (when fail-on-error
                            (return-from l:forward nil)))))
    (l:get l c)))

(defun l:backward (l a &optional (c nil c-supplied-p))
  (if c-supplied-p
      (l:put l a c)
      (l:create l a)))

(defun l:make-lens (get &optional (create nil create-supplied-p)
                          (put nil put-supplied-p))
  (cond (put-supplied-p
         (l:lens get put create))
        (create-supplied-p
         (l:bij get create))
        (get
         (l:bij get))))

(-> l:lens ((-> (t) t)
            (-> (t t) t)
            &optional (or null (-> (t) t)))
  (values l:lens &optional))
(defun l:lens (get put &optional create)
  "Create a lens from GET, PUT, and CREATE functions.
If CREATE is not provided, it is treated as PUT with a concrete
argument of nil."
  (letrec ((l
            (make 'l:lens :get get
                          :put put
                          :create
                          (or create
                              (lambda (a)
                                (l:put l a nil))))))
    l))

(-> l:qlens ((-> (t) t)
             (-> (t t) t)
             (-> (t) t))
  (values l:qlens &optional))
(defun l:qlens (get put create)
  (make 'l:qlens :get get :put put :create create))

(-> l:bij ((function (t) t) &optional (function (t) t))
  (values l:bij &optional))
(defun l:bij (get &optional (create get))
  "Return a bijective lens from the given GET function and its inverse, CREATE.

Similar to \"bij\" from Hoffmann et al., 2010, except that BIJ takes both
directions of the bijection instead of just the forward direction."
  (make 'l:bij :get get :create create))

(def +identity-lens+ (l:bij #'identity #'identity))

(defun l:identity ()
  "Return the identity lens.

Same as \"id\" from Foster et al., 2007."
  +identity-lens+)

(defun l:identity! ()
  "Like l:identity, but returns T as its second value.
This allows it to work even on null values."
  (l:bij (lambda (x) (values x t))
         (lambda (x) (values x t))))

(-> l:compose (&rest l:qlens) (values l:qlens &optional))
(defun l:compose (&rest lenses)
  "Return a lens that abstracts through the LENSES in order.

Similar to the semicolon combinator from Foster et al., 2007, except that
COMPOSE acts like ALEXANDRIA:COMPOSE in that (in the forward direction) lenses
are applied from back to front rather than from front to back. That is, l;k is
the same as (compose k l).

Allows the composition of zero lenses, equivalent to the identity
lens."
  ;; not sure whether this is associative; if not, then this REDUCE approach to
  ;; composing more than two lenses may be inappropriate
  (reduce (lambda (k l)
            (make-instance 'l:lens
              :get (lambda (c) (l:get k (l:get l c)))
              :put (lambda (a c) (l:put l (l:put k a (l:get l c)) c))
              :create (lambda (a) (l:create l (l:create k a)))))
          lenses
          :initial-value (l:identity)))

(-> l:compose* (&rest l:qlens) (values l:qlens &optional))
(defun l:compose* (&rest lenses)
  "Like `l:compose', but discerning."
  ;; not sure whether this is associative; if not, then this REDUCE approach to
  ;; composing more than two lenses may be inappropriate
  (reduce (lambda (k l)
            (make-instance 'l:lens
              :get (lambda (c)
                     (multiple-value-bind (c c?)
                         (l:discern-values (l:get l c))
                       (and c? (l:get k c))))
              :put (lambda (a c)
                     (nest
                      (let ((c1 c)))
                      (multiple-value-bind (c c?)
                          (l:discern-values (l:get l c)))
                      (and c?)
                      (multiple-value-bind (a a?)
                          (l:discern-values (l:put k a c)))
                      (and a?)
                      (l:put l a c1)))
              :create (lambda (a)
                        (multiple-value-bind (a a?)
                            (l:discern-values (l:create k a))
                          (and a? (l:create l a))))))
          lenses
          :initial-value (l:identity)))

(defun l:opp (l)
  "Return a bijective lens that acts like L in reverse.

Completely discards all PUT behavior of L, making use only of GET and CREATE.

Similar to \"op\" from Hoffmann et al., 2010, except that OPP is not involutory
because the lenses defined in this package are not symmetric."
  (l:bij {l:create l} {l:get l}))

(defun l:constantly (v &optional (d nil dp))
  "Return a lens that always returns V.

If D is given, uses it as a default value for CREATE.

Similar to \"const\" from Foster et al., 2007, except that CONSTANTLY makes D
optional and simply omits the \"create\" function if D is not provided."
  (l:lens
   (constantly v)
   (lambda (a c)
     (declare (ignore a))
     (values c t))
   (and dp
        (lambda (&rest args)
          (declare (ignore args))
          (values d t)))))

(-> l:acond ((function (t) t) (function (t) t) l:qlens l:lens)
  (values l:lens &optional))
(defun l:acond (c1 a1 l1 l2)
  "Return a lens that applies L1 or L2 depending on whether C1 or A1 hold.

The domains of L1 and L2 must be disjoint, as must be the codomains of L1 and
L2. In the forward direction, applies the C1 predicate, then uses L1 if true or
L2 if false. In the reverse direction, applies the A1 predicate, then uses L1 if
true or L2 if false. Uses CREATE instead of PUT if C1 and A1 disagree in the
reverse direction.

Same as \"acond\" from Foster et al., 2007."
  (fbind (c1 a1)
    (l:lens (lambda (c) (if (c1 c) (l:get l1 c) (l:get l2 c)))
            (lambda (a c)
              (if (a1 a)
                  (if (c1 c) (l:put l1 a c) (l:create l1 a))
                  (if (c1 c) (l:create l2 a) (l:put l2 a c))))
            (lambda (a) (if (a1 a) (l:create l1 a) (l:create l2 a))))))

(-> l:lazy (function) (values l:lens &optional))
(defun l:lazy (f)
  "Return a lens that acts like the lens returned by F.

Doesn't call F until necessary, but also doesn't cache the result of
F. To get caching, compose this function with something like
SERAPEUM:ONCE."
  (fbind (f)
    (l:lens (lambda (c) (l:get (f) c))
            (lambda (a c) (l:put (f) a c))
            (lambda (a) (l:create (f) a)))))

(defun l:car (&optional (d nil dp))
  "Return a lens to extract the car of a cons cell.

If D is given, uses it as a default cdr value for CREATE.

Similar to \"hd\" from Foster et al., 2007, except that CAR makes D optional and
simply omits the \"create\" function if D is not provided."
  (l:lens (lambda (c)
            (and (listp c)
                 (values (car c) t)))
          (lambda (a c) (cons a (cdr c)))
          (and dp {cons _ d})))

(defun l:cdr (&optional (d nil dp))
  "Return a lens to extract the cdr of a cons cell.

If D is given, uses it as a default car value for CREATE.

Similar to \"tl\" from Foster et al., 2007, except that CDR makes D optional and
simply omits the \"create\" function if D is not provided."
  (l:lens (lambda (c)
            (and (listp c)
                 (values (cdr c) t)))
          (lambda (a c) (cons (car c) a))
          (and dp {cons d})))

(defun l:cons (l1 l2)
  "Return a lens that applies L1 to the car, and L2 to cdr, of a cons cell.

Inspired by \"xfork\" from Foster et al., 2007."
  (l:lens «cons [{l:get l1} #'car] [{l:get l2} #'cdr]»
          (lambda (a c)
            (cons (l:put l1 (car a) (car c))
                  (l:put l2 (cdr a) (cdr c))))
          «cons [{l:create l1} #'car] [{l:create l2} #'cdr]»))

(defun l:mapcar (l)
  "Return a lens that applies L to every element of a list.

Similar to \"list_map\" from Foster et al., 2007.

This is a discerning lens: L is assumed to be discerning, and if it
fails for any element, then the entire lens fails."
  (l:lens (lambda (c)
            (let ((result (mapcar (lambda (c)
                                    (l:discern (l:get l c)))
                                  c)))
              (when (every #'cdr result)
                (values (mapcar #'car result) t))))
          (lambda (a c)
            (if (not (listp c))
                (l:create (l:mapcar l) a)
                (let ((result
                        (append
                         (mapcar (op (l:discern (l:put l _ _)))
                                 a c)
                         (mapcar (op (l:discern (l:create l _)))
                                 (drop (length c) a)))))
                  (and (every #'cdr result)
                       (values (mapcar #'car result) t)))))
          (lambda (a)
            (let ((result (mapcar (lambda (a)
                                    (l:discern (l:create l a)))
                                  a)))
              (and (every #'cdr result)
                   (values (mapcar #'car result) t))))))

(defun l:some (l)
  "Like l:mapcar, but as soon as one result matches, use that."
  (l:make-lens (lambda (cs)
                 (dolist (c cs)
                   (let ((result (l:discern (l:get l c))))
                     (when (cdr result)
                       (return (car+cdr result))))))
               (lambda (as)
                 (dolist (a as)
                   (let ((result (l:discern (l:create l a))))
                     (when (cdr result)
                       (return (values (list (car result)) t))))))
               (lambda (as c)
                 (let ((tail
                         (iter (for tail on as)
                               (for a = (car tail))
                               (let ((result (l:discern (l:put l a c))))
                                 (when (cdr result)
                                   (return (cons (car result) tail)))))))
                   (append (ldiff as tail) tail)))))

(defun l:list (&rest lenses)
  "Return a lens that matches a sequence of lenses.
That is, it matches values that are sequences of the same length as
LENSES, if every lens in LENSES matches the element at the same
position in the value."
  (multiple-value-call #'l:list*
    (values-list lenses)
    (l:check #'null)))

(defmacro l:lazy-list (&rest lenses)
  "Like `l:list', but does not evaluate LENSES until the lens is called.
Note LENSES may be evaluated more than once!"
  `(l:list ,@(mapcar (op `(l:lazy (lambda () ,_))) lenses)))

(defun l:list* (&rest lenses)
  "Like `l:list', but the last lens matches a tail of unspecified
length."
  (flet ((all-or-none (fn xs)
           (fbind fn
             (and (typep xs 'sequence)
                  (length<= (1- (length lenses)) xs)
                  (iter (for tail on xs)
                        (for (l . more?) on lenses)
                        (if more?
                            (let ((result (l:discern (fn l (car tail)))))
                              (if (cdr result)
                                  (collect (car result))
                                  (return nil)))
                            (let ((result (l:discern (fn l tail))))
                              (if (cdr result)
                                  (appending (car result))
                                  (return nil)))))))))
    (l:bij {all-or-none #'l:get}
           {all-or-none #'l:create})))

(defmacro l:lazy-list* (&rest lenses)
  "Like `l:list*', but does not evaluate LENSES until the lens is called.
Note LENSES may be evaluated more than once!"
  `(l:list* ,@(mapcar (op `(l:lazy (lambda () ,_))) lenses)))

(defun l:path (path)
  "Return a lens to access the subtree of an FSet structure at PATH."
  (if (empty? path)
      (l:identity)
      (destructuring-bind (first . rest) (convert 'list path)
        (l:compose (l:path rest)
                   (l:lens {lookup _ first}
                           (lambda (a c) (with c first a)))))))

(defun l:nilable (l)
  "Return a discerning lens that acts like L but has no ill-formed values."
  (let ((f {values _ t}))
    (l:lens [f {l:get l}]
            [f {l:put l}]
            [f {l:create l}])))

(defun l:guard (pred lens)
  "Return a discerning lens whose well-formed concrete values all
satisfy PRED."
  (l:compose* lens (l:check pred)))

(defun l:rguard (pred lens)
  "Return a discerning lens whose well-formed abstract values all
satisfy PRED."
  (l:compose* (l:check pred) lens))

(defun l:check (pred)
  "Return a discerning identity lens whose well-formed values all satisfy PRED."
  (let ((f (lambda (x) (values x (and (funcall pred x) t)))))
    (l:bij f f)))

(defun l:satisfies (pred)
  (l:check pred))

;; Allow optimizing the type check.
(declaim (inline l:check-type l:of-type l:check-subtype))

(defun l:check-type (type)
  "Return a discering identity lens whose well-formed values are of TYPE."
  (l:check {typep _ type}))

(defun l:of-type (type)
  "Return a discering identity lens whose well-formed values are of TYPE."
  (l:check {typep _ type}))

(defun l:check-subtype (type)
  "Return a discering identity lens whose well-formed values are
subtypes of TYPE."
  (l:check {subtypep _ type}))

(-> l:or (&rest l:qlens) (values l:qlens &optional))
(defun l:or (&rest lenses)
  "Return a lens that tries LENSES in order until one succeeds.
A lens succeeds if the last value it returns is not `nil'."
  (cond ((null lenses)
         (l:bij (constantly nil) (constantly nil)))
        ((single lenses) (first lenses))
        (t
         (l:lens
          (lambda (c)
            (iter (for l in lenses)
                  (for (values a a?) = (l:discern-values (l:get l c)))
                  (when a?
                    (return (values a a?)))))
          (lambda (a c)
            (iter (for l in lenses)
                  (for (values new-c c?) = (l:discern-values (l:put l a c)))
                  (when c?
                    (return (values new-c c?)))))
          (lambda (a)
            (iter (for l in lenses)
                  (for (values new-c c?) = (l:discern-values (l:create l a)))
                  (when c?
                    (return (values new-c c?)))))))))

(-> l:or! (&rest l:qlens) (values l:qlens &optional))
(defun l:or! (&rest lenses)
  "Like `l:or', but strict.
If more than one lens in LENSES matches, that's an error."
  (letrec ((lens
            (flet ((handle-results (results)
                     (case (count-if #'cdr results)
                       (0 nil)
                       (1 (values (car (find-if #'cdr results))
                                  t))
                       (t (error 'lens-error
                                 :format-control
                                 (formatter "More than one match for ~a")
                                 :format-arguments (list lens))))))
              (l:lens
               (lambda (c)
                 (handle-results
                  (mapcar (op (l:discern (l:get _ c)))
                          lenses)))
               (lambda (a c)
                 (handle-results
                  (mapcar (op (l:discern (l:put _ a c)))
                          lenses)))
               (lambda (a)
                 (handle-results
                  (mapcar (op (l:discern (l:create _ a)))
                          lenses)))))))
    lens))

(defun l:combine (combiner lens)
  "Convert a bijection into a lens.

Return a lens that, in the PUT direction, uses COMBINER, a function,
to augment the value constructed by LENS with the old value.

Converts a bijection into a lens by using COMBINER to \"patch\" the
result of `create' with the old concrete value."
  (fbind combiner
    (l:lens {l:get lens}
            (lambda (a c)
              (combiner (l:put lens a c) c))
            {l:create lens})))

(-> l:mapping
  (l:qlens
   &key
   (:maker (-> (t) t))
   (:getter (-> (t t) t))
   (:setter (-> (t t t) t))
   (:walker (-> ((-> (t t) t) t) t)))
  (values l:qlens &optional))
(defun l:mapping (lens &key
                         (maker (required-argument :maker))
                         (getter (required-argument :getter))
                         (setter (required-argument :setter))
                         (walker (required-argument :walker)))
  "Low-level lens that maps LENS over some kind of key-value mapping (table).

If you are writing lenses for translation, you probably want to use
`alist-mapping', `hash-table-mapping', or `fset-mapping'. This
function is lower-level, for supporting new tabular data structures in
a consistent way.

In the GET direction, the new lens returns an abstract table where the
keys are the same as the concrete table, but the values are the result
of passing each value through LENS.

In the CREATE direction, the new lens returns a concrete table with
the same keys (domain) as the abstract table, and values derived by
passing the values through LENS.

In the PUT direction, the new lens also returns a concrete table with
the same keys as the abstract table, but the values depend on which
keys are also present in the concrete table. If the same key is
present in both tables, then the new concrete value is computed using
PUT with the abstract value and the old concrete value; otherwise
CREATE is used.

Note that it is always the case (by design) that keys that are present
in the concrete table but not the abstract table are lost.

Takes four callbacks:

1. MAKER is a function of one argument that creates a new, empty
   mapping that is \"like\" its argument \(e.g. a hash table having
   the same test.)

2. GETTER takes a table and a key and returns two values \(like
   `gethash', but the table comes before the key).

3. SETTER takes a table, a key, and a value, and returns a table
   updated with the new key and value. It is fine if the table is
   mutated, but the return value has to be the table.

4. WALKER takes a table and a binary function and maps it over the
   entries in the table (like `maphash')."
  (fbind (maker getter setter walker)
    (declare (ftype (-> (function t) t) walker)
             (ftype (-> (t t) t) getter)
             (ftype (-> (t t t) t) setter)
             (ftype (-> (t) t) maker))
    (l:lens (lambda (c)
              (let ((a (maker c)))
                (walker
                 (lambda (k v)
                   (setf a (setter a k (l:get lens v))))
                 c)
                a))
            (lambda (a c)
              (let ((c2 (maker c)))
                (walker
                 (lambda (k v)
                   (nest
                    (setf c2)
                    (setter c2 k)
                    (multiple-value-bind (c c?) (getter c k))
                    (if c?
                        (l:put lens v c)
                        (l:create lens v))))
                 a)
                c2))
            (lambda (a)
              (let ((c (maker a)))
                (walker
                 (lambda (k v)
                   (setf c (setter c k (l:create lens v))))
                 a)
                c)))))

(defun l:hash-table-mapping (l)
  "Lens that maps L over the values of a hash table.
See `l:mapping' for the details."
  (l:mapping l
             :maker
             (lambda (h)
               (make-hash-table :test (hash-table-test h)))
             :getter (flip #'gethash)
             :setter
             (lambda (h k v)
               (setf (gethash k h) v)
               h)
             :walker #'maphash))

(defun l:alist-mapping (l &key (test #'eql))
  "Lens that maps L over the values of an alist.
TEST is the equality predicate on keys."
  (l:mapping l
             :maker (constantly nil)
             :getter
             (lambda (a k)
               (assocdr k a :test test))
             :setter
             (lambda (a k v)
               (acons k v (remove k a :key #'car :test test)))
             :walker
             (lambda (fn alist)
               (fbind fn
                 (iter (for (k . v) in alist)
                       (fn k v))))))

(defun l:fset-mapping (l)
  "Lens that maps L over the values of an FSet map."
  (l:mapping l
             :maker (constantly (empty-map))
             :getter #'lookup
             :setter #'with
             :walker
             (lambda (fn map)
               (fbind fn
                 (iter (for (k v) in-map map)
                       (fn k v))))))

(defun l:hash-table-alist (&key (test #'eql))
  "Lens that translates between hash tables and alists.
In the PUT direction, the hash table test is taken from the old hash
table; otherwise TEST is used."
  (l:combine
   (lambda (alist old)
     (if (null alist) nil
         (if (null old)
             (alist-hash-table alist :test test)
             (alist-hash-table
              alist
              :test (hash-table-test old)
              :size (hash-table-size old)))))
   (l:bij #'hash-table-alist #'identity)))

(defun l:default (x)
  "As a function, identity.

As a Trivia pattern, matches anything."
  x)

(defpattern l:default (x)
  "Match anything."
  `(or ,x _))

(defun split-bindings (bindings)
  "Split a two-element list into a list of the first elements and a
list of the second elements."
  (iter (for (v b) in bindings)
        (collect v into vs)
        (collect b into bs)
        (finally (return (values vs bs)))))

(defun lookup-default (col key default)
  "Like `fset:lookup', but return DEFAULT if KEY was not present.
The second value is T if the key was present, NIL otherwise."
  (multiple-value-bind (val val?) (lookup col key)
    (if val?
        (values val t)
        (values default nil))))

(def %concrete-defaults (empty-map)
  "Fallback global lexical binding for a variable that various macros
  expect to be locally bound lexically.")

(defun extract-defaults (bindings)
  (values
   (mapcar (op (take 2 _)) bindings)
   (mapcar (op (list (first _1) (third _1)))
           (keep 3 bindings :key #'length))))

(defpattern l:seq (&rest ps)
  "Match an FSet seq.
Defined here to avoid stomping on `fset:seq' itself."
  (with-unique-names (it)
    `(guard1 (,it :type seq)
             (typep ,it 'seq)
             (size ,it) ,(length ps)
             ,@(iter (for p in ps)
                     (for i from 0)
                     (collect `(lookup ,it ,i))
                     (collect p)))))

(defmacro l:seq (&rest items)
  "Same as `fset:seq'."
  `(fset:seq ,@items))

(defpattern l:map (&rest kvs)
  "Match an FSet map.
Defined here to avoid stomping on `fset:map' itself."
  (with-unique-names (it)
    `(guard1 (,it :type fset:map)
             (typep ,it 'fset:map)
             ,@(iter (for (k v) in kvs)
                     (for i from 0)
                     (collect `(lookup ,it ,k))
                     (collect v)))))

(defmacro l:map (&rest kvs)
  "Same as `fset:map'."
  `(fset:map ,@kvs))

(defvar *trace* nil "Are we tracing?")

(defun l:trace ()
  "Trace (successful) lens matching."
  (setf *trace* t))

(defun l:untrace ()
  "Stop tracing lens matching."
  (setf *trace* nil))

(defvar *depth* 0 "Depth of lens matching.")

(defun maybe-trace (direction names lens-forms &rest results)
  "If tracing, print the results of successful lens matching."
  (declare (dynamic-extent results))
  (mvlet ((values results (halves results)))
    (when *trace*
      (iter (for name in names)
            (for value in values)
            (for lens-form in lens-forms)
            (for result in results)
            (format *trace-output*
                    "~&~vt~<~2i~:(~a~) @~a ~a -> ~a~:@>~%"
                    (* 2 *depth*)
                    (list direction
                          (list name lens-form)
                          (handler-case
                              (fmt "~s" value)
                            (error ()
                              "UNPRINTABLE"))
                          (handler-case
                              (fmt "~s" result)
                            (error ()
                              "UNPRINTABLE"))))))))

(defconst +unbound+ '+unbound+
  "Distinguished value that makes a binding morally unbound.")

(defun morally-unbound? (sym env)
  "Return non-nil if SYM is (morally) unbound in ENV."
  (eql +unbound+ (macroexpand-1 sym env)))

(defmacro with-morally-unbound-variables ((&rest vars) &body body)
  "Bind VARS around BODY in such a way that they are morally unbound."
  `(symbol-macrolet ,(mapcar (op `(,_ ,+unbound+)) vars)
     ,@body))

(defmacro l:with-component-lenses ((&rest bindings) fn &body body
                                   &environment env)
  "A helper macro for `l:match'.

The BINDINGS are similar to those of a LET: each one is a list whose
first element is a symbol and whose second element is a form that
evaluates to a discerning lens.

FN should be one of `l:get', `l:create', or `l:put'. (If you are using
`l:put' directly you many want to establish component-wise defaults
using `l:with-defaults').

Within BODY, the variables in BINDINGS are rebound to the result of
calling the requested lens on the existing value of that binding,
using FN.

If any lens is ill-formed (returns nil as its last value) then BODY is
not evaluated and the entire form returns nil."
  (with-unique-names (bail)
    (mvlet* ((vars lenses (split-bindings bindings))
             (temps (make-gensym-list (length vars))))
      `(let ((*depth* (1+ *depth*)))    ;For tracing.
         (block ,bail
           (multiple-value-bind ,temps
               (values
                ,@(iter (for var in vars)
                        (for lens in lenses)
                        (collect
                            `(multiple-value-bind (,var valid?)
                                 (l:discern-values
                                  ,(ecase fn
                                     (l:get
                                      `(,fn ,lens ,var))
                                     (l:create
                                      (if (morally-unbound? var env)
                                          `(lookup %concrete-defaults ',var)
                                          `(,fn ,lens ,var)))
                                     (l:put
                                      ;; If there is no abstract
                                      ;; component, use the
                                      ;; concrete component value
                                      ;; (default or derived)
                                      ;; directly. Otherwise invoke
                                      ;; `put' on the abstract
                                      ;; component with the derived
                                      ;; concrete value.
                                      (if (morally-unbound? var env)
                                          `(lookup %concrete-defaults
                                                   ',var)
                                          `(,fn ,lens ,var
                                             (lookup %concrete-defaults
                                                     ',var))))))
                               (if valid?
                                   ,var
                                   (return-from ,bail nil))))))
             ,@(unsplice
                (when temps
                  `(maybe-trace ',(if (symbolp fn) fn 'l:put)
                                ',vars
                                ',lenses
                                ,@vars
                                ,@temps)))
             (let ,(mapcar #'list vars temps)
               ,@(when (eql fn 'l:get)
                   ;; TODO Only allow the get direction to ignore
                   ;; defaulted variables.
                   `((declare (ignorable ,@vars))))
               ,@body)))))))

(defmacro l:with-get-lenses ((&rest bindings) &body body)
  "Same as `l:with-component-lenses' with `l:get' as its function."
  `(l:with-component-lenses ,bindings
                            l:get
     ,@body))

(defmacro l:with-create-lenses ((&rest bindings) &body body)
  "Same as `l:with-component-lenses' with `l:create' as its function."
  `(l:with-component-lenses ,bindings
                            l:create
     ,@body))

(defmacro l:with-put-lenses ((&rest bindings) &body body)
  "Same as `l:with-component-lenses' with `l:put' as its function."
  `(l:with-component-lenses ,bindings
                            l:put
     ,@body))

(defmacro match-two (bindings &body (c-form a-form)
                     &environment env)
  "Actually construct a lens from a pair of concrete and abstract forms."
  (nest
   (flet ((maybe-expand (pat)
            "If PAT is a list that begins with $, macroexpand it."
            (match pat
              ((list '$ pat) (macroexpand-1 pat env))
              (otherwise pat)))))
   (let ((c-form (assure (not null) (maybe-expand c-form)))
         (a-form (assure (not null) (maybe-expand a-form)))))
   (with-unique-names (a c))
   (mvlet* ((bindings defaults (extract-defaults bindings))
            (vars (mapcar #'car bindings))
            (defaulted-vars default-values (split-bindings defaults))))
   `(nest
     ;; `%concrete-defaults' is an internal symbol other macros expect
     ;; to be bound. It serves two purposes: it contains the defaults
     ;; to be used in the concrete direction when the abstract pattern
     ;; does not bind variables the concrete form expects, AND, only
     ;; for `put', it holds the actual concrete values extracted from
     ;; the original concrete object.
     (let ((%concrete-defaults
             (fset:map ,@(mapcar (op `(',_ ,_))
                                 defaulted-vars default-values)))))
     (assure l:lens)
     (l:lens
      ;; We use `named-lambda' for debugging purposes.
      (named-lambda ,(gensym "get") (,c)
        (let ,(mapcar #'list defaulted-vars default-values)
          (declare (ignorable ,@defaulted-vars))
          (match ,c
            ;; Match against the concrete form.
            (,c-form
             (l:with-get-lenses ,bindings
               ;; Evaluate the abstract form.
               (values ,a-form t))))))
      (named-lambda ,(gensym "put") (,a ,c)
        (let ((%concrete-defaults
                ;; Try to compute concrete components from the old
                ;; concrete value.
                (let ,(mapcar #'list defaulted-vars default-values)
                  (declare (ignorable ,@defaulted-vars))
                  (ematch ,c
                    ;; Match against the concrete form.
                    (,c-form
                     ;; Build a map from the bindings.
                     (fset:map ,@(mapcar (op `(',(car _1) ,(car _1)))
                                         bindings)))
                    (otherwise %concrete-defaults)))))
          (declare (ignorable %concrete-defaults))
          (with-morally-unbound-variables ,vars
            (match ,a
              ;; Match against the abstract form.
              (,a-form
               (l:with-put-lenses ,bindings
                 ;; "Ignorable" defaulted vars.
                 (progn ,@defaulted-vars)
                 ;; Evaluate the concrete form.
                 (values ,c-form t)))))))
      (named-lambda ,(gensym "create") (,a)
        (with-morally-unbound-variables ,vars
          (match ,a
            ;; Match against the abstract form.
            (,a-form
             (l:with-create-lenses ,bindings
               ;; "Ignorable" defaulted vars
               (progn ,@defaulted-vars)
               ;; Evaluate the concrete form.
               (values ,c-form t))))))))))

(defmacro l:match (bindings &body patterns)
  "Return a discerning lens using BINDINGS to translate between PATTERNS.

PATTERNS is a list where every two elements, a concrete form and an
abstract form, defines one lens.

Each multiple of two patterns is compiled into a separate lens, and
the lens are implicitly combined with `l:or'.

If there is only one pattern, it is repeated. (This is an abbreviation
for when the concrete and abstract forms are the same.)

If any pattern is of the form `($ PAT)', then PAT is macroexpanded and
the expansion is treated as the real pattern.

The BINDINGS are similar to those of a LET: each one is a list whose
first element is a symbol and whose second element is a form that
evaluates to a discerning lens. There may also be a third element,
which is used a default in the `create' direction.

The concrete form is a Trivia pattern (using the symbols from
BINDINGS) that would evaluate to some concrete structure when given
appropriate values for each of the symbols in BINDINGS.

Likewise the abstract form is a Trivia pattern, using the symbols from
BINDINGS, that would evaluate to some abstract structure when given
appropriate values.

Note that the symbols used by the concrete form/pattern and the
symbols used by the abstract form/pattern do not need to be the same;
the only requirement is that they both be subsets of the variables
bound in BINDINGS.

If the concrete form/pattern omits variables, they are always
defaulted with the provided defaults.

If the abstract pattern omits variables, the way the concrete form
evaluates differs between `create' and `put'.

If the concrete form is being evaluated in the `create' direction,
then missing symbols are filled in from provided defaults in the
bindings of the `l:match' form.

If the concrete form is being evaluated in the `put' direction, then
missing symbols are filled in from the provided member of the concrete
domain (by matching it against the concrete pattern with `get').

The GET direction first attempts to find values for the symbols in
BINDINGS that would make the concrete pattern match the input. If
no such values can be found, NIL is returned. Otherwise, those values
are each sent through their respective lenses defined by BINDINGS, and
if each of those lenses produced well-formed values, their outputs are
used to construct the final output via the abstract form. The CREATE
direction is analogous."
  ;; Allow a single pattern as a shorthand for the same pattern twice.
  (when (single patterns)
    (return-from l:match
      `(l:match ,bindings ,@patterns ,@patterns)))
  `(l:or
    ,@(iter (for (pat1 pat2) in (batches patterns 2 :even t))
            (collect `(match-two ,bindings ,pat1 ,pat2)))))

(defun lensify (x)
  (if (typep x 'l:lens) x
      (l:default x)))

(defmacro l:match-constructors (bindings &body patterns)
  `(l:match ,(mapcar (lambda (b)
                       (if (listp b) b
                           (list b '(l:identity))))
                     bindings)
     ,@patterns))

(defmacro l:match-list (&body bindings)
  "Like `l:match' for translating from list to list.
If each list has the same length, and that length is the same as the
length of the bindings, there is no need to spell it out."
  `(l:match ,bindings
     (list ,@(mapcar #'car bindings))))

(defmacro l:match-list* (&body bindings)
  "Like `l:match-list', but the last binding is applied to the tail,
instead of per-element."
  `(l:match ,bindings
     (list* ,@(mapcar #'car bindings))))

(defun l:optional (x)
  "Return a lens that matches whatever X matches, or nil.
X must not match nil itself."
  (l:or! (l:check-type 'null) x))
