(defpackage :fresnel/test/util
  (:documentation
   "A package defining the root suite for all Fresnel tests.")
  (:use #:gt/full
        #:stefil+)
  (:export :test))
(in-package :fresnel/test/util)
(in-readtable :curry-compose-reader-macros)

(defroot test)
