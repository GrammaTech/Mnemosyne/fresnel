(defpackage :fresnel/test/lens
  (:use :gt/full
        :stefil+
        :trivial-package-local-nicknames
        :fresnel/test/util)
  ;; (:local-nicknames (:l :fresnel/lens))
  (:import-from :fresnel/lens)
  (:shadowing-import-from :fresnel/fresnel :seq)
  (:export :test))
(in-package :fresnel/test/lens)
(in-readtable :curry-compose-reader-macros)
(defsuite test-lens "Lenses.")

(eval-always
  (add-package-local-nickname :l :fresnel/lens))

(defun plus-one-lens ()
  "A lens that adds 1 to a number in the forward direction and subtracts 1 in
the reverse direction."
  (l:bij #'1+ #'1-))

(deftest test-bij ()
  (let ((l (plus-one-lens)))
    (is (equal? 4 (l:get l 3)))
    ;; this lens is bijective, so PUT should ignore its second argument
    (is (equal? 1 (l:put l 2 5)))
    (is (equal? 6 (l:create l 7)))))

(deftest test-lens-laws ()
  ;; This isn't really a test; it's more just showing how to phrase the lens
  ;; laws in the syntax of the :FRESNEL/LENS package, concretized with a specific
  ;; lens and some specific C and A values as an example.
  (let ((l (plus-one-lens))
        (c 5)
        (a 12))
    (is (equal? c (l:put l (l:get l c) c)))    ; GetPut
    (is (equal? a (l:get l (l:put l a c))))    ; PutGet
    (is (equal? a (l:get l (l:create l a)))))) ; CreateGet

(deftest test-identity ()
  (let ((l (l:identity)))
    (is (equal? :foo (l:get l :foo)))
    (is (equal? :bar (l:put l :bar :baz))) ; ignore previous concrete value
    (is (equal? :qux (l:create l :qux)))))

(deftest test-compose ()
  ;; nullary composition is the identity
  (let ((l (l:compose)))
    (is (equal? :foo (l:get l :foo)))
    (is (equal? :bar (l:put l :bar :baz)))
    (is (equal? :qux (l:create l :qux))))
  ;; unary composition is the same lens
  (let ((l (l:compose (plus-one-lens))))
    (is (equal? 4 (l:get l 3)))
    (is (equal? 1 (l:put l 2 5)))
    (is (equal? 6 (l:create l 7))))
  (let ((l (l:compose (plus-one-lens) (l:car))))
    (is (equal? 5 (l:get l '(4 . :foo))))
    (is (equal? '(2 . :bar) (l:put l 3 '(1 . :bar))))
    ;; (signals error (l:create l 6))
    ) ; no default cdr value provided to L:CAR
  (let ((l (l:compose (l:constantly :foo 42) (plus-one-lens) (l:car :baz))))
    (is (equal? :foo (l:get l '(4 . :qux))))
    (is (equal? '(2 . :quux) (l:put l :foo '(2 . :quux))))
    (is (equal? '(41 . :baz) (l:create l :foo)))))

(deftest test-op ()
  (let ((l (l:opp (plus-one-lens))))       ; bijective example
    (is (equal? 3 (l:get l 4)))
    (is (equal? 2 (l:put l 1 5)))
    (is (equal? 7 (l:create l 6))))
  (let ((l (l:opp (l:constantly :foo :bar)))) ; non-bijective example
    (is (equal? :bar (l:get l :baz)))
    (is (equal? :foo (l:put l :qux :quux)))
    (is (equal? :foo (l:create l :norf)))))

(deftest test-constantly ()
  (let ((l (l:constantly :foo)))
    (is (equal? :foo (l:get l :baz)))
    (is (equal? :qux (l:put l :foo :qux)))
    ;; (signals error (l:create l :foo))
    ) ; no default value, so no CREATE
  (let ((l (l:constantly :foo nil)))
    (is (equal? :foo (l:get l :baz)))
    (is (equal? :qux (l:put l :foo :qux)))
    (is (null (l:create l :foo)))))

(deftest test-acond ()
  ;; In the language of Foster et al., 2007, for this lens, A1 is the set of
  ;; keywords, A\A1 is the set of numbers, C1 is the set of cons cells whose CAR
  ;; is a keyword, and C\C1 is the set of numbers (the same as A\A1).
  (let ((l (l:acond #'consp #'keywordp (l:car :foo) (plus-one-lens))))
    (is (equal? :bar (l:get l '(:bar . :baz))))
    ;; C1 and A1 agree, so use PUT
    (is (equal? '(:qux . :baz) (l:put l :qux '(:bar . :baz))))
    ;; use CREATE instead of PUT, since C1 and A1 disagree
    (is (equal? '(:qux . :foo) (l:put l :qux 3)))
    (is (equal? '(:qux . :foo) (l:create l :qux)))
    (is (equal? 4 (l:get l 3)))
    ;; C1 and A1 agree, so use PUT (although we can't tell the difference here)
    (is (equal? 5 (l:put l 6 3)))
    ;; use CREATE instead of PUT, since C1 and A1 disagree
    (is (equal? 5 (l:put l 6 '(:bar . :baz))))
    (is (equal? 5 (l:create l 6))))
  ;; this next one is to catch a bug that the earlier one didn't catch, which
  ;; only shows up if L2 is not bijective
  (let ((l (l:acond {equal? :foo} {equal? :bar}
                    (l:constantly :bar :foo)
                    (l:constantly nil nil))))
    (is (equal? nil (l:get l :baz)))
    (is (equal? :bar (l:get l :foo)))
    ;; cases that the above ACOND lens example doesn't catch:
    (is (equal? :baz (l:put l :qux :baz))) ; this one
    (is (equal? nil (l:put l :qux :foo)))  ; and this one
    (is (equal? :foo (l:put l :bar :baz)))
    (is (equal? :foo (l:put l :bar :foo)))
    (is (equal? nil (l:create l :qux)))
    (is (equal? :foo (l:create l :bar)))))

(deftest test-lazy ()
  (labels (;; silly lens that acts like MAPCAR but for CDR
           (mapcdr (l)
             (l:acond #'null #'null
               (l:identity)
               ;; would be an infinite loop without LAZY
               (l:cons (l:lazy {mapcdr l}) l))))
    (let ((l (mapcdr (plus-one-lens))))
      (is (equal? '(((nil . 4) . 3) . 2)
                  (l:get l '(((nil . 3) . 2) . 1))))
      (is (equal? '(((nil . 3) . 2) . 1)
                  (l:put l '(((nil . 4) . 3) . 2) '(nil . 5))))
      (is (equal? '(((nil . 2) . 1) . 0)
                  (l:create l '(((nil . 3) . 2) . 1)))))))

(deftest test-car ()
  (let ((l (l:car)))
    (is (equal? :foo (l:get l '(:foo . :bar))))
    (is (equal? '(:foo . :baz) (l:put l :foo '(:bar . :baz))))
    ;; (signals error (l:create l :foo))
    ) ; no default value, so no CREATE
  (let ((l (l:car nil)))
    (is (equal? :foo (l:get l '(:foo . :bar))))
    (is (equal? '(:foo . :baz) (l:put l :foo '(:bar . :baz))))
    (is (equal? '(:foo . nil) (l:create l :foo)))))

(deftest test-cdr ()
  (let ((l (l:cdr)))
    (is (equal? :bar (l:get l '(:foo . :bar))))
    (is (equal? '(:bar . :foo) (l:put l :foo '(:bar . :baz))))
    ;; (signals error (l:create l :foo))
    ) ; no default value, so no CREATE
  (let ((l (l:cdr nil)))
    (is (equal? :bar (l:get l '(:foo . :bar))))
    (is (equal? '(:bar . :foo) (l:put l :foo '(:bar . :baz))))
    (is (equal? '(nil . :foo) (l:create l :foo)))))

(deftest test-cons ()
  (let ((l (l:cons (plus-one-lens) (l:constantly :foo :bar))))
    (is (equal? '(4 . :foo) (l:get l '(3 . :baz))))
    (is (equal? '(5 . :baz) (l:put l '(6 . :foo) '(3 . :baz))))
    (is (equal? '(5 . :bar) (l:create l '(6 . :foo))))))

(deftest test-mapcar ()
  (let ((l (l:mapcar (plus-one-lens))))
    (is (equal? nil (l:get l nil)))
    (is (equal? nil (l:put l nil '(1 2 3)))) ; bijective: ignore C
    (is (equal? nil (l:create l nil)))
    (is (equal? '(2 3 4) (l:get l '(1 2 3))))
    (is (equal? '(8 7 6 5) (l:put l '(9 8 7 6) '(1 2 3)))) ; same here
    (is (equal? '(8 7 6 5) (l:create l '(9 8 7 6))))))

(deftest test-path ()
  ;; example taken from ClojureDocs:
  ;; https://clojuredocs.org/clojure.core/assoc-in
  (let ((users (seq (fset:map (:name "James") (:age 26))
                    (fset:map (:name "John") (:age 43))))
        (l (l:path (seq 1 :age))))
    (is (equal? 43 (l:get l users)))
    (is (equal? (seq (fset:map (:name "James") (:age 26))
                     (fset:map (:name "John") (:age 44)))
                (l:put l 44 users)))))

(deftest test-discern ()
  (is (equal? '(nil . nil) (l:discern nil)))
  (is (equal? '(:foo . t) (l:discern :foo)))
  (is (equal? '(nil . nil) (l:discern (values nil nil))))
  (is (equal? '(:foo . nil) (l:discern (values :foo nil))))
  (is (equal? '(nil . t) (l:discern (values nil :bar))))
  (is (equal? '(:foo . t) (l:discern (values :foo :bar))))
  ;; more than 2 values aren't currently used here but who knows, maybe someday
  (is (equal? '(nil . nil) (l:discern (values nil nil nil))))
  (is (equal? '(:foo . nil) (l:discern (values :foo nil nil))))
  (is (equal? '(nil . nil) (l:discern (values nil :bar nil))))
  (is (equal? '(:foo . nil) (l:discern (values :foo :bar nil))))
  (is (equal? '(nil . t) (l:discern (values nil nil :baz))))
  (is (equal? '(:foo . t) (l:discern (values :foo nil :baz))))
  (is (equal? '(nil . t) (l:discern (values nil :bar :baz))))
  (is (equal? '(:foo . t) (l:discern (values :foo :bar :baz)))))

(deftest test-nilable ()
  (let ((l (l:nilable (l:constantly nil nil))))
    (is (equal? '(nil . t) (l:discern (l:get l :foo))))
    (is (equal? '(nil . t) (l:discern (l:put l :bar nil))))
    (is (equal? '(:qux . t) (l:discern (l:put l :baz :qux))))
    (is (equal? '(nil . t) (l:discern (l:create l :quux))))))

(deftest test-check ()
  (let ((l (l:check #'listp)))
    (is (equal? '(t . nil) (l:discern (l:get l t))))
    (is (equal? '(3 . nil) (l:discern (l:get l 3))))
    (is (equal? '(nil . t) (l:discern (l:get l nil))))
    (is (equal? '((1 2) . t) (l:discern (l:get l '(1 2)))))
    ;; CREATE is exactly the same as GET
    (is (equal? '(t . nil) (l:discern (l:create l t))))
    (is (equal? '(3 . nil) (l:discern (l:create l 3))))
    (is (equal? '(nil . t) (l:discern (l:create l nil))))
    (is (equal? '((1 2) . t) (l:discern (l:create l '(1 2)))))
    ;; PUT acts just like CREATE
    (is (equal? '(t . nil) (l:discern (l:put l t t))))
    (is (equal? '(t . nil) (l:discern (l:put l t 3))))
    (is (equal? '(t . nil) (l:discern (l:put l t nil))))
    (is (equal? '(t . nil) (l:discern (l:put l t '(1 2)))))
    (is (equal? '(3 . nil) (l:discern (l:put l 3 t))))
    (is (equal? '(3 . nil) (l:discern (l:put l 3 3))))
    (is (equal? '(3 . nil) (l:discern (l:put l 3 nil))))
    (is (equal? '(3 . nil) (l:discern (l:put l 3 '(1 2)))))
    (is (equal? '(nil . t) (l:discern (l:put l nil t))))
    (is (equal? '(nil . t) (l:discern (l:put l nil 3))))
    (is (equal? '(nil . t) (l:discern (l:put l nil nil))))
    (is (equal? '(nil . t) (l:discern (l:put l nil '(1 2)))))
    (is (equal? '((1 2) . t) (l:discern (l:put l '(1 2) t))))
    (is (equal? '((1 2) . t) (l:discern (l:put l '(1 2) 3))))
    (is (equal? '((1 2) . t) (l:discern (l:put l '(1 2) nil))))
    (is (equal? '((1 2) . t) (l:discern (l:put l '(1 2) '(1 2)))))))

(defun safe-plus-one-lens ()
  "A discerning lens like PLUS-ONE-LENS whose well-formed values are numbers."
  (l:acond #'numberp  #'numberp (plus-one-lens) (l:or)))

(deftest test-match/no-ill-formed ()
  ;; can't have an ill-formed value as the pattern
  (signals error (eval '(l:match () nil nil)))
  (signals error (eval '(l:match () :foo nil)))
  (signals error (eval '(l:match () nil :bar))))

(deftest test-match/single-unique ()
  ;; single unique matching values
  (let ((l (l:match () :foo :bar)))
    (is (equal? '(nil . nil) (l:discern (l:get l nil))))
    (is (equal? '(nil . nil) (l:discern (l:get l :baz))))
    (is (equal? '(:bar . t) (l:discern (l:get l :foo))))
    (is (equal? '(nil . nil) (l:discern (l:put l nil nil))))
    ;; (is (equal? '(:baz . nil) (l:discern (l:put l nil :baz))))
    (is (equal? '(nil . nil) (l:discern (l:put l nil :foo))))
    (is (equal? '(nil . nil) (l:discern (l:put l :qux nil))))
    ;; (is (equal? '(:baz . nil) (l:discern (l:put l :qux :baz))))
    (is (equal? '(nil . nil) (l:discern (l:put l :qux :foo))))
    (is (equal? '(:foo . t) (l:discern (l:put l :bar nil))))
    (is (equal? '(:foo . t) (l:discern (l:put l :bar :baz))))
    (is (equal? '(:foo . t) (l:discern (l:put l :bar :foo))))
    (is (equal? '(nil . nil) (l:discern (l:create l nil))))
    (is (equal? '(nil . nil) (l:discern (l:create l :qux))))
    (is (equal? '(:foo . t) (l:discern (l:create l :bar))))))

(deftest test-match/wrap-discerning ()
  ;; wrapping a single (discerning) lens acts almost like that lens, but PUT
  ;; gives more information when neither argument is well-formed
  (let ((l (l:match ((x (safe-plus-one-lens))) x x)))
    (is (equal? '(nil . nil) (l:discern (l:get l nil))))
    (is (equal? '(nil . nil) (l:discern (l:get l :foo))))
    (is (equal? '(4 . t) (l:discern (l:get l 3))))
    (is (equal? '(nil . nil) (l:discern (l:put (safe-plus-one-lens) :foo :bar))))
    ;; (is (equal? '(:bar . nil) (l:discern (l:put l :foo :bar))))
                                        ; different
    (is (equal? '(8 . t) (l:discern (l:put l 9 :bar))))
    (is (equal? '(nil . nil) (l:discern (l:put l :foo 6))))
    (is (equal? '(2 . t) (l:discern (l:put l 3 0))))
    (is (equal? '(nil . nil) (l:discern (l:create l nil))))
    (is (equal? '(nil . nil) (l:discern (l:create l :bar))))
    (is (equal? '(1 . t) (l:discern (l:create l 2))))))

(deftest test-match/default ()
  ;; DEFAULT pattern
  (let ((l (l:match ()
             ;; DEFAULT allows other patterns nested within it
             (l:default (seq :foo))
             ;; putting it in a SEQ so that it can be NIL (not root pattern)
             (seq (l:default :bar)))))
    ;; NIL isn't allowed at the root
    ;; (is (equal? '(nil . nil) (l:discern (l:get l nil))))
    ;; but anything else is, due to the DEFAULT
    (is (equal? (cons (seq :bar) t) (l:discern (l:get l 42))))
    (is (equal? (cons (seq :bar) t) (l:discern (l:get l :foo))))
    (is (equal? (cons (seq :bar) t) (l:discern (l:get l (seq :baz)))))
    (is (equal? (cons (seq :bar) t) (l:discern (l:get l (seq :foo)))))
    ;; NIL is allowed since it isn't the root pattern
    (is (equal? (cons (seq :bar) t) (l:discern (l:get l (seq nil)))))
    (is (equal? (cons (seq :foo) t) (l:discern (l:create l (seq nil)))))
    (is (equal? (cons (seq :foo) t) (l:discern (l:create l (seq 42)))))
    (is (equal? (cons (seq :foo) t) (l:discern (l:create l (seq :bar)))))))

(deftest test-match/non-unique ()
  ;; non-unique matching values, using defaults
  (let ((l (l:match () (l:seq) (l:map))))
    (is (equal? '(nil . nil) (l:discern (l:get l nil))))
    (is (equal? '(nil . nil) (l:discern (l:get l :foo))))
    (is (equal? '(nil . nil) (l:discern (l:get l (empty-map)))))
    (is (equal? '(nil . nil) (l:discern (l:get l (fset:map (:foo :bar))))))
    (is (equal? (cons (empty-map) t) (l:discern (l:get l (empty-seq)))))
    ;; (is (equal? (cons (empty-map) t) (l:discern (l:get l (l:seq :foo :bar)))))
    (is (equal? '(nil . nil) (l:discern (l:put l nil nil))))
    ;; (is (equal? (cons (fset:map (:foo :bar)) nil)
    ;;             (l:discern (l:put l nil (fset:map (:foo :bar))))))
    (is (equal? '(nil . nil) (l:discern (l:put l nil (seq :foo :bar)))))
    (is (equal? '(nil . nil) (l:discern (l:put l (seq :baz :qux) nil))))
    ;; (is (equal? (cons (fset:map (:foo :bar)) nil)
    ;;             (l:discern (l:put l (seq :baz :qux) (fset:map (:foo :bar))))))
    (is (equal? '(nil . nil)
                (l:discern (l:put l (seq :baz :qux) (seq :foo :bar)))))
    (is (equal? (cons (empty-seq) t)
                (l:discern (l:put l (fset:map (:baz :qux)) nil))))
    (is (equal? (cons (empty-seq) t)
                (l:discern (l:put l (empty-map) (fset:map (:foo :bar))))))
    (is (equal? (cons (empty-seq) t)
                (l:discern (l:put l (fset:map (:baz :qux)) (empty-seq)))))
    ;; TODO: test PUT for FSET:SEQ and FSET:MAP patterns
    #+(or)
    (is (equal? (cons (seq :foo :bar) t)
                (l:discern (l:put l (fset:map (:baz :qux)) (seq :foo :bar)))))
    (is (equal? '(nil . nil) (l:discern (l:create l nil))))
    (is (equal? '(nil . nil) (l:discern (l:create l :baz))))
    (is (equal? '(nil . nil) (l:discern (l:create l (empty-seq)))))
    (is (equal? '(nil . nil) (l:discern (l:create l (seq :baz :qux)))))
    (is (equal? (cons (empty-seq) t) (l:discern (l:create l (empty-map)))))
    (is (equal? (cons (empty-seq) t)
                (l:discern (l:create l (fset:map (:baz :qux))))))))

(let ((l1 (safe-plus-one-lens))
      (l3 (l:mapcar (plus-one-lens))))

  (deftest test-or-nullary ()
    ;; nullary always returns NIL for everything
    (let ((l (l:or)))
      (is (null (l:get l :foo)))
      (is (null (l:put l :bar :baz))) ; different from (l:constantly nil nil)
      (is (null (l:create l :qux)))))

  (deftest test-or-unary ()
    ;; unary just gives back the same lens
    (let ((l (l:or l1)))
      (is (equal? nil (l:get l nil)))
      (is (equal? nil (l:get l :foo)))
      (is (equal? 4 (l:get l 3)))
      (is (equal? nil (l:create l nil)))
      (is (equal? nil (l:create l :bar)))
      (is (equal? 1 (l:create l 2)))))

  (deftest test-or-domain-1 ()
    (let ((l (l:or l1 l3))) ; "domain" only includes numbers and lists of them
      (is (equal? 5 (l:get l 4)))       ; short-circuiting
      (is (equal? '(2 3 4) (l:get l '(1 2 3))))
      (is (equal? nil (l:get l nil)))
      (signals error (l:get l :foo))    ; outside our "domain"
      (is (equal? 7 (l:create l 8)))    ; short-circuiting
      (is (equal? '(4 5 6) (l:create l '(5 6 7))))
      (is (equal? nil (l:create l nil)))
      (signals error (l:create l :bar))))) ; also outside the "domain")

(deftest test-or-domain-2 ()
  (let ((l1 (safe-plus-one-lens))
        (l2 (l:acond #'cdr #'cdr (l:or) (l:identity)))
        (l3 (l:mapcar (plus-one-lens))))
    (let ((l (l:or l1 l2 l3)))
      (is (equal? 6 (l:get l 5)))
      (is (equal? '(42) (l:get l '(42))))
      (is (equal? '(3 4 5) (l:get l '(2 3 4))))
      (is (equal? nil (l:get l nil)))
      (signals error (l:get l :foo))
      (is (equal? 0 (l:create l 1)))
      (is (equal? '(42) (l:create l '(42))))
      (is (equal? '(5 6 7) (l:create l '(6 7 8))))
      (is (equal? nil (l:create l nil)))
      (signals error (l:create l :bar)))))

(deftest test-hash-table-mapping ()
  (let ((l (l:hash-table-mapping (l:bij #'1+ #'1-))))
    (is (equal (hash-table-alist (l:get l (dict :x 1)))
               '((:x . 2))))
    (is (eql 'equal
             (hash-table-test
              (l:put l
                     (dict 'eql :x 1)
                     (dict :x 1)))))
    (is (eql 'eql (hash-table-test (l:create l (dict 'eql :x 2)))))))

(deftest test-alist-mapping ()
  (let ((l (l:alist-mapping (l:bij #'1+ #'1-))))
    (is (equal '((:x . 2)) (l:get l '((:x . 1)))))
    (is (equal '((:x . 1)) (l:create l '((:x . 2)))))))

(deftest test-hash-table-alist ()
  (is (equal
       (l:get (l:hash-table-alist)
              (dict :x 1))
       '((:x . 1))))
  (is (eql 'equal
           (hash-table-test
            (l:put (l:hash-table-alist)
                   '((:x . 1))
                   (dict :x 1))))))

(defmacro readme-test (&body body)
  "Define tests in a style that can easily be copy-pasted into a readme."
  `(local
     ,@(nlet rec ((body body)
                  (acc nil))
         (match body
           ((list) (nreverse acc))
           ((list* expr '=> result body)
            (rec body
                 (cons `(is (equal? ,expr ,result)) acc)))
           ((list* form body)
            (rec body (cons form acc)))))))

(deftest test-lit-to-vector ()
  (readme-test
    (def list-to-vector (l:bij {coerce _ 'list} {coerce _ 'vector}))
    (l:get list-to-vector '(1 2 3)) => #(1 2 3)
    (l:create list-to-vector #(1 2 3)) => '(1 2 3)
    (def vector-to-list (l:opp list-to-vector))

    (l:get vector-to-list #(1 2 3)) => '(1 2 3)
    (l:create vector-to-list '(1 2 3)) => #(1 2 3)))

(deftest test-address ()
  (readme-test
    (def address-book
      '(("Pat"
         ("Phone" . "555-4444")
         ("URL" . "http://pat.example.com"))
        ("Chris"
         ("Phone" . "555-9999")
         ("URL" . "http://chris.example.org"))))
    (def abstract1
      '(("Chris" . "555-9999") ("Pat" . "555-4444")))
    (def abstract2
      '(("Pat" . "555-4321")
        ("Jo" . "555-6666")))
    (def lens
      (l:alist-mapping
       (l:match ((phone (l:identity))
                 (url (l:identity) "http://google.com"))
         (list
          (cons "Phone" phone)
          (cons "URL" url))
         phone)
       :test #'equal))

    (l:get lens address-book)
    => abstract1

    (l:put lens abstract2 address-book)
    => '(("Jo"
          ("Phone" . "555-6666")
          ("URL" . "http://google.com"))
         ("Pat"
          ("Phone" . "555-4321")
          ("URL" . "http://pat.example.com")))))

(deftest test-walkthrough ()
  (readme-test
    (def c
      '(("Phone" . "555-4444")
        ("URL" . "http://pat.example.com")))

    (def l
      (l:match
          ;; The components.
          ((phone (l:identity))
           (url (l:identity) "http://google.com"))
        ;; The concrete form.
        (list
         (cons "Phone" phone)
         (cons "URL" url))
        ;; The abstract form.
        phone))

    (l:get l c) => "555-4444"
    (l:create l "555-4444")
    => '(("Phone" . "555-4444")
         ("URL" . "http://google.com"))
    (l:put l "555-4444" c)
    => '(("Phone" . "555-4444")
         ("URL" . "http://pat.example.com"))))

(eval-always
  (defclass provenance ()
    ((children :initarg :children)
     (src :initarg :src))))

(defun prov-lens ()
  (l:or
   (l:match ((src (l:identity))
             (children (l:mapcar (prov-lens))))
     (make 'provenance :children children :src src)
     children)
   (l:identity)))

(deftest test-restore-provenance ()
  (let ((p (make 'provenance :children '(x) :src 'y)))
    (is (equal '(x) (l:get (prov-lens) p)))
    (let ((p2 (l:put (prov-lens) '(x) p)))
      (is (eql 'y (slot-value p2 'src))))))

(deftest test-restore-nested-provenance ()
  (let ((p (make 'provenance
                 :src 'place0
                 :children (list
                            (make 'provenance
                                  :children '(a)
                                  :src 'place1)
                            (make 'provenance
                                  :children '(b)
                                  :src 'place2)))))
    (is (equal '((a) (b))
               (l:get (prov-lens) p)))
    (let ((p2 (l:put (prov-lens) '((a) (b)) p)))
      (is (eql 'place0 (slot-value p2 'src)))
      (destructuring-bind (c1 c2) (slot-value p2 'children)
        (is (eql 'place1 (slot-value c1 'src)))
        (is (eql 'place2 (slot-value c2 'src)))))))

(defun flip-lens ()
  (l:match ((x (l:identity!))
            (y (l:identity!)))
             (list x y)
             (list y x)))

(deftest test-flip ()
  "Test that identity! matches nil."
  (is (equal '(2 1) (l:get (flip-lens) '(1 2))))
  (is (equal '(nil nil) (l:get (flip-lens) '(nil nil))))
  (is (equal '(1 nil) (l:get (flip-lens) '(nil 1))))
  (is (equal '(nil 2) (l:get (flip-lens) '(2 nil)))))
