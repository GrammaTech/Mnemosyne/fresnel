Contributing
============


## Code of Conduct

Please read the [Mnemosyne Code of Conduct](CODE_OF_CONDUCT.md).

## General Guidelines

- Text files may not have trailing whitespace.

- Text files must end with a trailing newline.

- All tests should be able to run and pass.
  This can be checked by running `make check` on your build directory.

- All files shall be properly formatted as determined by our
  `pre-commit` configuration (see below for details).

- We ask that all contributors complete our Contributor License
  Agreement (CLA), which can be found at
  [GrammaTech-CLA-mnemosyne.pdf](./GrammaTech-CLA-mnemosyne.pdf), and
  email the completed form to `CLA@GrammaTech.com`.  Under this
  agreement contributors retain the copyright to their work but grants
  GrammaTech unlimited license to the work.

### pre-commit

In general, code must follow a unified format. To make compliance with
this format easier, we recommend the use of
[`pre-commit`](https://pre-commit.com/) with the provided
configuration file, `.pre-commit-config.yaml`, to manage formatting.
To use `pre-commit`:

1. If `pre-commit` is not already installed on your system, install it
   now with [`pip`](https://pypi.org/project/pip/).
   ```shell
      pip3 install pre-commit
   ```
2. Install [`lisp-format`](https://github.com/eschulte/lisp-format)
   and ensure that your `~/.lisp-formatrc` file configures Emacs
   (e.g., by adding directories to your `load-path` and/or by calling
   the `package-initialize` function) so that you can load slime and
   paredit.
3. Install the formatters as a pre-commit hook. In the project root directory:
   ```shell
    pre-commit install
   ```
   If you prefer to run `pre-commit` manually instead, run this before all commits:
   ```shell
   pre-commit run
   ```

## Common Lisp Code Requirements

This project uses the
[SEL coding standards](https://grammatech.github.io/sel/SEL-Coding-Standards.html).

### Testing Development

- All code you care about should be tested.
- Any code you don't care about should be removed.
- No unit test should take more than 0.5 seconds.

## Python Code Requirements

- Code must be [PEP8](https://www.python.org/dev/peps/pep-0008/) compliant.
  To check for PEP8 compliance, [flake8](https://pypi.org/project/flake8/) is recommended,
  and included as part of our `pre-commit` configuration.

- All code must be formatted with [Black](https://pypi.org/project/black/)
  (set to line lengths of 79, for PEP8 compliance).
  A pass through this tool is included as part of our `pre-commit` configuration.
  - Please note that `black` only works on Python version 3.6 and newer.
    This is newer than what is available on some OSes by default (for example, Ubuntu 16),
    so you may have to install Python 3.6 or newer to run `black`.
    If installing Python 3.6+ on your system is not possible, there exists
    [an online interface to Black](https://black.now.sh/?version=stable&state=_Td6WFoAAATm1rRGAgAhARYAAAB0L-Wj4AA-ACxdAD2IimZxl1N_W1ktIvcnCRyzdeeGA586U8RMKbisP9D6xUd8v4usX-jR3lIAACNC8ndFJAQXAAFIPxtdQK4ftvN9AQAAAAAEWVo=)
    you can manually enter files into.

- The Python API should be made to run on all version of Python 3.

- Use `UpperCamelCase` for type names, `UPPER_CASE` for constant names,
  and `snake_case` for other identifier names.

### Testing Development

- All code you care about should be tested.
- Any code you don't care about should be removed.
- Code testing is done via the built-in `unittest` framework.
- No unit test should take more than 0.5 seconds.

## Documentation

Documentation for the Mnemosyne project at large is located in the
[documentation repository](https://gitlab.com/GrammaTech/Mnemosyne/docs)
where it should be maintained.
