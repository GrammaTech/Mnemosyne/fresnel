# mostly copied from functional-trees:
# https://github.com/GrammaTech/functional-trees/blob/8ed658b/Makefile

# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

PACKAGE_NAME = fresnel
LIST_DEPS = $(wildcard *.lisp)

include .cl-make/cl.mk
