(uiop:define-package :fresnel/fresnel
  (:use :gt/full
        :trivial-package-local-nicknames)
  ;; (:local-nicknames (:l :fresnel/lens))
  (:import-from :fresnel/lens)
  (:shadowing-import-from :fresnel/lens :seq :map)
  (:export
    :read-print-lens
    :python-precedence-canonizer
    :terminal-symbol-lens
    :print-read-lens
    :gensym-lens
    :keyword-lens
    :gensym?
    :alist-lens)
  (:documentation "Very high-level lenses; nothing language-specific."))
(in-package :fresnel/fresnel)
(in-readtable :curry-compose-reader-macros)

(eval-always
  (add-package-local-nickname :l :fresnel/lens))

(defun print-read-lens (type &rest args &key)
  "Like `read-print-lens', but translates between concrete values and
abstract strings."
  (l:opp (apply #'read-print-lens type args)))

(defgeneric read-print-lens (type &key &allow-other-keys)
  (:documentation "Construct a lens to translate between concrete
  strings and abstract values of type TYPE, using the appropriate Lisp
  reader/parser and printer.")
  (:method ((type (eql 'integer)) &key)
    (build-read-print-lens
     'integer
     #'parse-number
     #'princ-to-string))
  (:method ((type (eql 'number)) &key)
    (l:or (read-print-lens 'integer)
          (read-print-lens 'double-float)))
  (:method ((type (eql 'float)) &key (format 'double-float))
    (assert (subtypep format 'float))
    (build-read-print-lens
     format
     (lambda (s)
       (let ((*read-default-float-format* format))
         (parse-number s)))
     (lambda (f)
       (let ((*read-default-float-format* format))
         (princ-to-string f)))))
  (:method ((type (eql 'double-float)) &key)
    (read-print-lens 'float :format 'double-float))
  (:method ((type (eql 'single-float)) &key)
    (read-print-lens 'float :format 'single-float))
  (:method ((type (eql 'symbol)) &key package)
    (build-read-print-lens
     type
     (if (null package)
         #'make-symbol
         (lambda (s)
           (values (intern s package) t)))
     #'princ-to-string)))

(defun build-read-print-lens (type parser unparser)
  "Construct a bijection between a concrete string and an abstract
value of TYPE using a function PARSER and a function UNPARSER.

The lens only matches if the value returned by PARSER is of TYPE.
Errors of type `parse-error' from PARSER are also considered
failures."
  (fbind (parser unparser)
    (l:bij (lambda (x)
             (and (stringp x)
                  (let ((result
                          (ignore-some-conditions (parse-error)
                            (parser x))))
                    (values result (typep result type)))))
           (lambda (x)
             (and (typep x type)
                  (unparser x))))))

(defun gensym-lens ()
  "Lens to convert between a concrete string to an abstract gensym."
  (l:bij (lambda (x)
           (when (and (stringp x)
                      (not (emptyp x)))
             (make-symbol x)))
         (lambda (x)
           (when (and x
                      (symbolp x)
                      (no (symbol-package x)))
             (symbol-name x)))))

(defun keyword-lens ()
  "Lens to convert between a concrete string and an abstract keyword."
  (l:bij (lambda (x)
           (when (and (stringp x)
                      (not (emptyp x)))
             (values (make-keyword x) t)))
         (lambda (x)
           (when (keywordp x)
             (symbol-name x)))))

(defun gensym? (x)
  "Is X a symbol without a package?"
  (and (symbolp x)
       (null (symbol-package x))))

(defun alist-lens (alist &rest args &key &allow-other-keys)
  "Wrap an association list as a bijective lens.
GET on the lens becomes assoc, and CREATE on the lens becomes rassoc."
  (l:bij (lambda (c)
           (let ((cons (apply #'assoc c alist args)))
             (and cons
                  (values (cdr cons) cons))))
         (lambda (a)
           (let ((cons (apply #'rassoc a alist args)))
             (and cons
                  (values (car cons) cons))))))
