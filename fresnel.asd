(defsystem "fresnel"
  :name "fresnel"
  :author "GrammaTech"
  :licence "MIT"
  :description "Bidirectional translation with lenses"
  :depends-on ("fare-quasiquote-extras"
               "trivial-package-local-nicknames"
               "fresnel/fresnel")
  :class :package-inferred-system
  :build-operation "asdf:program-op"
  :build-pathname "fresnel"
  :in-order-to ((test-op (load-op "fresnel/test")))
  :perform (test-op (o c) (symbol-call :fresnel/test '#:run-batch)))

(defsystem "fresnel/readtable"
  :author "GrammaTech"
  :licence "MIT"
  :depends-on ("fare-quasiquote-extras" "cl-interpol" "gt/full")
  :components ((:file "fresnel-readtable")))

(register-system-packages "trivia" '(:trivia.fail))
