(defpackage :fresnel/test
  (:documentation
   "An entry point to run all tests for the :FRESNEL system: call (RUN-BATCH).

Each package testing a specific source package must include the following in its
DEFPACKAGE form, in order to ensure that its test suite is included in the root
Fresnel suite:

    (:use #:fresnel/test/util)")
  (:use #:gt/full
        #:stefil+
        #:fresnel/test/util)
  ;; must import all packages from the test directory
  (:import-from :fresnel/test/lens))
(in-package :fresnel/test)
(in-readtable :curry-compose-reader-macros)
