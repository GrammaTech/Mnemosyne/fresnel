(defpackage :fresnel/readtable
  (:use :gt/full)
  (:import-from :fare-quasiquote)
  (:import-from :cl-interpol)
  (:export :lens-readtable)
  (:documentation "Convenience readtable for defining lenses.

Notably uses fare-quasiquote for pattern matching with backquotes, and
CL-INTERPOL so we can use C-style escapes \n in strings."))
(in-package :fresnel/readtable)

(defreadtable lens-readtable
  (:fuse
   :standard
   :curry-compose-reader-macros
   :interpol-syntax
   :fare-quasiquote))
